import numpy as np
import datetime
import copy
from collections import namedtuple

p_base = namedtuple('parameters',
                    ['horizon',
                     'ess_cap',  # Wh
                     'ess_init_lvl',
                     'ess_pcap_ratio',  # practical capacity
                     'ess_ch_rate',
                     'forecast_err'],
                    defaults=[24, 6000, 2000, 0.9, 1/3, 50])


class Output:
    def __init__(self, param, demand, generation):
        self.param = param
        self.demand = demand
        self.generation = generation
        self.on_grid = np.zeros(param.horizon)
        self.gen_curt = np.zeros(param.horizon)
        self.ess_ch = np.zeros(param.horizon)
        self.ess_dch = np.zeros(param.horizon)
        self.ess_lvl = np.zeros(param.horizon)
        self.ess_schedule = np.zeros((param.horizon, 2))

    def saveOutput(self, file_path=None):
        # output
        if file_path is None:
            save_time = datetime.datetime.today().strftime("%y%m%d_%H%M%S")
            file_path = f'./results/run_{save_time}.csv'
        np.savetxt(file_path, np.dstack([time, self.demand, self.generation,
                                         self.on_grid, self.gen_curt, self.ess_ch, self.ess_dch, self.ess_lvl,
                                         self.ess_schedule[:, 0],self.ess_schedule[:, 1]])[0],
                   delimiter=',', header='time,demand,generation,on_grid,gen_curt,ess_ch,ess_dch,ess_lvl,'
                                         'ess_schedule_ch,ess_schedule_dch', fmt='%.8f')


class ESS:
    def __init__(self, capacity, init_lvl, max_ch, max_dch):
        self.capacity = capacity
        self.lvl = init_lvl
        self.max_ch = max_ch
        self.max_dch = max_dch

    def Run(self, net, ch, dch):
        if self.lvl < dch:
            raise ValueError('Discharging power exceeds current ESS level')
        elif self.capacity - self.lvl < ch:
            raise ValueError('Charging power exceeds ESS capacity')

        if dch > self.max_dch:
            raise ValueError('Discharging power exceeds the discharge capability of ESS')
        elif ch > self.max_ch:
            raise ValueError('Charging power exceeds the charge capability of ESS')

        self.lvl = self.lvl + ch - dch
        net = net + ch - dch

        return net, self.lvl

    def autoRun(self, net):
        ch_lvl = 0
        dch_lvl = 0

        if net > 0:  # 모자름 방전해야됨 (discharge)
            avail_lvl = min(self.max_dch, self.lvl)  # 방전가능량
            if net <= avail_lvl:  # ess로 커버가능
                self.lvl = self.lvl - net
                dch_lvl = net
                net = 0
            else:  # ess로 커버 불가능
                self.lvl = self.lvl - avail_lvl
                dch_lvl = avail_lvl
                net = net - avail_lvl  # 가능한만큼 활용
        else:  # 남음 충전해야됨 (charge)
            avail_lvl = min(self.max_ch, self.capacity - self.lvl)  # 충전 가능량
            if -net <= avail_lvl:  # ess에 모두 충전 가능
                self.lvl = self.lvl - net
                ch_lvl = -net
                net = 0
            else:  # ess에 일부만 충전 가능
                self.lvl = self.lvl + avail_lvl
                ch_lvl = avail_lvl
                net = net + avail_lvl

        return net, ch_lvl, dch_lvl, self.lvl


def ESS_Scheduler(demand, generation, ess):
    on_grid = np.zeros(param.horizon)
    gen_curt = np.zeros(param.horizon)
    ess_ch = np.zeros(param.horizon)
    ess_dch = np.zeros(param.horizon)
    ess_lvl = np.zeros(param.horizon)
    for t in range(param.horizon):
        net = demand[t] - generation[t]
        net, ess_ch[t], ess_dch[t], ess_lvl[t] = ess.autoRun(net)
        if net > 0:  # 외부에서 사와야됨
            on_grid[t] = net
        else:  # 전력 버려짐
            gen_curt[t] = -net

    return on_grid, gen_curt, ess_ch, ess_dch, ess_lvl


def Sim(demand, generation, ess, ess_schedule):
    on_grid = np.zeros(param.horizon)
    gen_curt = np.zeros(param.horizon)
    ess_lvl = np.zeros(param.horizon)
    for t in range(param.horizon):
        net = demand[t] - generation[t]
        net, ess_lvl[t] = ess.Run(net, ess_schedule[t,0], ess_schedule[t,1])
        if net > 0:  # 외부에서 사와야됨
            on_grid[t] = net
        else:  # 전력 버려짐
            gen_curt[t] = -net

    return on_grid, gen_curt, ess_schedule[:,0], ess_schedule[:,1], ess_lvl


if __name__ == '__main__':
    # Read data (Wh)
    time, demand, generation = np.loadtxt('data/24_hour_data.csv', skiprows=1, delimiter=',', unpack=True, dtype=float)
    # parameters
    param = p_base()
    # output
    o = Output(param, demand, generation)

    # forecast module
    demand_forecast = copy.deepcopy(demand)
    generation_forecast = copy.deepcopy(generation)

    # add forecast error
    err = np.random.normal(0.0, param.forecast_err, len(demand_forecast))
    demand_forecast = np.clip(demand_forecast + err, 0, np.inf)
    err = np.random.normal(0.0, param.forecast_err, len(demand_forecast))
    generation_forecast = np.clip(generation_forecast + err, 0, np.inf)

    # init ess
    ess_max_ch = param.ess_cap * param.ess_pcap_ratio * param.ess_ch_rate
    ess_max_dch = param.ess_cap * param.ess_pcap_ratio * param.ess_ch_rate
    ess_tmp = ESS(param.ess_cap, param.ess_init_lvl, ess_max_ch, ess_max_dch)
    ess = ESS(param.ess_cap, param.ess_init_lvl, ess_max_ch, ess_max_dch)

    # Run ESS schduler
    ess_schedule = np.zeros([param.horizon, 2])  # ch, dch
    _, _, ess_schedule[:,0], ess_schedule[:,1], _ = ESS_Scheduler(demand_forecast, generation_forecast, ess_tmp)
    o.ess_schedule = ess_schedule

    # Run Sim
    o.on_grid, o.gen_curt, o.ess_ch, o.ess_dch, o.ess_lvl = Sim(demand, generation, ess, ess_schedule)

    o.saveOutput()


